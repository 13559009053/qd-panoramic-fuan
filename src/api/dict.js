import http from '@/utils/request'

// 根据字典类型查询字典数据信息
export async function getDicts(params) {
  return await http.getRestApi("/api/dictData/find", params)
}

export default {
  /**  
   * 查询字典类型列表  
   */
  async getDictTypeListAllApi(params) {
    return await http.get("/api/dictType/listAll", params);
  },
  /**  
   * 分页查询字典类型列表  
   */
  async getDictTypeListApi(params) {
    return await http.get("/api/dictType/list", params);
  },
  // 根据字典类型查询字典数据信息
  async getDicts(params){
    return await http.getRestApi("/api/dictData/find", params);
   },
  /**  
   * 分页查询字典数据列表  
   */
  async getDictDataListApi(params) {
    return await http.get("/api/dictData/list", params);
  },
  /**
   * 添加字典类型
   */
  async addDictType(params){
    return await http.post("/api/dictType/add",params);
  },
  /**
   * 编辑字典类型
   */
  async updateDictType(params){
    return await http.put("/api/dictType/update",params);
  },
  /**
    * 检查字典类型下是否有子表
    */
  async checkDictType(params){
    return await http.getRestApi("/api/dictType/check", params);
   },
  /**
   * 删除字典类型
   */
  async deleteDictType(params){
    return await http.delete("/api/dictType/delete",params);
  },
  /**
   * 添加字典类型
   */
  async addDictData(params){
    return await http.post("/api/dictData/add",params);
  },
  /**
   * 编辑字典类型
   */
  async updateDictData(params){
    return await http.put("/api/dictData/update",params);
  },
  /**
   * 删除字典类型
   */
  async deleteDictData(params){
    return await http.delete("/api/dictData/delete",params);
  },
}
