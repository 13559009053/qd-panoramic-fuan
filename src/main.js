import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'
// import enLang from 'element-ui/lib/locale/lang/en'// 如果使用中文语言包请默认支持，无需额外引入，请删除该依赖

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

// //谷歌监听事件警告消除
// import 'default-passive-events'


//引入阿里巴巴图标库
import '@/assets/assets/font/iconfont.css'
import '@/assets/assets/font/iconfont.js'


import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

Vue.use(Viewer)

Viewer.setDefaults({
  'inline': false,
  'button': true, // 右上角按钮
  'navbar': false, // 底部缩略图
  'title': false, // 当前图片标题
  'toolbar': false, // 底部工具栏
  'tooltip': false, // 显示缩放百分比
  'movable': true, // 是否可以移动
  'zoomable': true, // 是否可以缩放
  'rotatable': false, // 是否可旋转
  'scalable': false, // 是否可翻转
  'transition': true, // 使用 CSS3 过度
  'fullscreen': true, // 播放时是否全屏
  'keyboard': true // 是否支持键盘
})

//导入按钮权限判断
import hasPermission from '@/permission/index'
Vue.prototype.hasPermission = hasPermission;

//导入封装信息确认提示框组件脚本
import myconfirm from '@/utils/myconfirm'
Vue.prototype.$myconfirm = myconfirm;

//导入清空表单工具
import resetForm from '@/utils/resetForm'
Vue.prototype.$resetForm = resetForm;

//导入快速复制对象工具
import objCopy from '@/utils/objCopy'
Vue.prototype.$objCopy = objCopy;

//导入时间格式化工具
import timestampToTime from '@/utils/timestampToTime'
Vue.prototype.$timestampToTime = timestampToTime;
import timestampToTime2 from '@/utils/timestampToTime2'
Vue.prototype.$timestampToTime2 = timestampToTime2;

//字典管理
import { getDicts } from "@/api/dict";
// 字典标签组件
import DictTag from '@/components/DictTag'
// 全局组件挂载
Vue.component('DictTag', DictTag)
// 字典数据组件
import DictData from '@/components/DictData'
DictData.install()

Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
// locale: enLang // 如果使用中文，无需设置，请删除
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
